package com.example.lootapp.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import com.example.lootapp.R
import com.example.lootapp.database.ProfileDatabaseRepository
import com.example.lootapp.models.Profile
import kotlinx.android.synthetic.main.activity_in_depth_page.*
import kotlinx.android.synthetic.main.change_password.view.*
import kotlinx.android.synthetic.main.change_username.view.*
import kotlinx.android.synthetic.main.change_username.view.submit

class InDepthPage : AppCompatActivity(){
    // Initialize variables
    val id : Double = Math.random()
    val profile = Profile(id, "David Nsoesi", "dnsoes1", "password", 3)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_in_depth_page)


        // Get database instance and values
        var profileX = ProfileDatabaseRepository(this)
        // Add profile to database
        profileX.addProfile(profile)

        // Set the values for profile data
        name.setText(profileX.getProfile(0).name)
        username.setText(profileX.getProfile(0).username)
        password.setText(profileX.getProfile(0).password)
        noAcountsValue.setText(profileX.getProfile(0).numOfAccounts.toString())

        // Implement onClick listener
        uploadImage.setOnClickListener {
            // Check runtime permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED){
                    // Permission denied
                    val permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE);
                    // Show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE);
                }
                else{
                    // Permission already granted
                    pickImageFromGallery();
                }
            }
            else{
                // System OS is < Marshmallow
                pickImageFromGallery();
            }
        }

        // Open change username dialog
        userNameBtn.setOnClickListener {
            // Inflate the dialog
            val dialogView = LayoutInflater.from(this).inflate(R.layout.change_username, null)

            // AlertDialogBuilder
            val builder = AlertDialog.Builder(this)
                .setView(dialogView)
                .setTitle("Change username")

            // Show dialog
            val alertDialog = builder.show()

            // Submit button click listener in dialog
            dialogView.submit.setOnClickListener {
                alertDialog.dismiss()

                // Update database
                username.setText(dialogView.newUser.text.toString())
            }
        }

        // Open change password dialog
        passwordBtn.setOnClickListener {
            // Inflate the dialog
            val dialogView = LayoutInflater.from(this).inflate(R.layout.change_password, null)

            // AlertDialogBuilder
            val builder = AlertDialog.Builder(this)
                .setView(dialogView)
                .setTitle("Change password")

            // Show dialog
            val alertDialog = builder.show()

            // Submit button click listener in dialog
            dialogView.submit.setOnClickListener {
                alertDialog.dismiss()
                val temp: Profile = Profile(id, "David Nsoesi","dnsoes1" ,"password", 3)
                password.setText(dialogView.newPass.text.toString())
            }
        }

    }


    private fun pickImageFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)

    }

    companion object {
        // Image picking code
        private val IMAGE_PICK_CODE = 1000

        // Permission code
        private val PERMISSION_CODE = 1001
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            PERMISSION_CODE -> {
                if(grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    pickImageFromGallery()
                }
                else {
                    // permission from popup denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            profilePicture.setImageURI(data?.data)
        }
    }

}
