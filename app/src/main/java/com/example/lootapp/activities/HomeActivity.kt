package com.example.lootapp.activities

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.lootapp.R
import com.example.lootapp.api.API_Interactor
import com.example.lootapp.fragments.BillsFragment
import com.example.lootapp.fragments.TransactionsFragment
import com.example.lootapp.models.UserAccountInfo
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.lang.Exception

class HomeActivity : AppCompatActivity(){

    // Initialize variables
    val job = Job()
    val ioScope = CoroutineScope(Dispatchers.IO + job)
    lateinit var notificationManager : NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    private val channelId = "com.example.lootapp.activities"
    private val description = "3 new suggestions"



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        // Fetch account information
        ioScope.launch {
            val profile = API_Interactor.fetchUserAccountInfo("dnsoes1")

        }



        // Notifications
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val intent = Intent(this, SuggestionsActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(this, 0,intent,PendingIntent.FLAG_UPDATE_CURRENT)

            // Setting up the notifcation
            notificationChannel = NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)

            // Builds notification
            builder = Notification.Builder(this, channelId)
                .setContentTitle("Loot App")
                .setContentText("You have 3 new suggestions")
                .setSmallIcon(R.drawable.ic_launcher_round)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources,R.drawable.ic_launcher))
                .setContentIntent(pendingIntent)
        } else {

            val pendingIntent = PendingIntent.getActivity(this, 0,intent,PendingIntent.FLAG_UPDATE_CURRENT)
            builder = Notification.Builder(this)
                .setContentTitle("Loot App")
                .setContentText("You have 3 new suggestions")
                .setSmallIcon(R.drawable.ic_launcher_round)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources,R.drawable.ic_launcher))
                .setContentIntent(pendingIntent)
        }
        notificationManager.notify(1234,builder.build())

        // Creating instances of fragments
        val transactionFragment = TransactionsFragment()
        val billsFragment = BillsFragment()


        // Used to replace content of the FrameLayout
        supportFragmentManager.beginTransaction().apply {
            // Sets initial fragment
            replace(R.id.flFragment, transactionFragment)
            commit()
        }

        // Allows for display of transaction fragment
        transactionsButton.setOnClickListener{
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flFragment, transactionFragment)
                commit()
            }
        }

        // Go to in depth page
        profileButton.setOnClickListener {
            val intent = Intent(this, InDepthPage::class.java)
            startActivity(intent)
        }

        // Allows for display of suggestions fragment
        suggestionsButton.setOnClickListener {
            val intent = Intent(this, SuggestionsActivity::class.java)
            startActivity(intent)
        }

        // Allows for display of bills fragment
        billsButton.setOnClickListener {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flFragment, billsFragment)
                commit()
            }
        }

    }

}
