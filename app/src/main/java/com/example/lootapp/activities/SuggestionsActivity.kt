package com.example.lootapp.activities

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.lootapp.R
import com.example.lootapp.adapters.SuggestionsPagerAdapter
import kotlinx.android.synthetic.main.activity_suggestions.*
import kotlinx.android.synthetic.main.fragment_suggestion_two.*

class SuggestionsActivity : AppCompatActivity() {

    lateinit var notificationManager : NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    private val channelId = "com.example.lootapp.activities"
    private val description = "Test Description"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_suggestions)

        // Reps tabs adapter
        val adapter = SuggestionsPagerAdapter(supportFragmentManager)
        suggestionsVP.adapter = adapter

        val fragmentAdapter = SuggestionsPagerAdapter(supportFragmentManager)
        suggestionsVP.adapter = fragmentAdapter
        tabs.setupWithViewPager(suggestionsVP)
    }

}
