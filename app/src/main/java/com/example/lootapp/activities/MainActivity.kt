package com.example.lootapp.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.example.lootapp.R
import com.example.lootapp.api.API_Interactor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.Serializable

class MainActivity : AppCompatActivity() {
    val job = Job()
    val ioScope = CoroutineScope(Dispatchers.IO + job)

//    lateinit var interactor: API_Interactor;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_main)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    // Navigates to Home Activity when login button is pressed
    fun loginButtonPressed(view: View){
        val intent = Intent(this, HomeActivity::class.java)
        val username = findViewById<EditText>(R.id.editText).text.toString()
        val password = findViewById<EditText>(R.id.editText3).text.toString()

        if((username.length < 1 || password.length < 1) || (!username.equals("dnsoes1") || (!password.equals("password")) )) {
            Log.v("Main Activity", "Username or password is null")
            val dialogView = LayoutInflater.from(this).inflate(R.layout.login_failure, null)
            val builder = AlertDialog.Builder(this)
                .setView(dialogView)
                .setTitle("Login Failed")

            val alertDialog = builder.show()
        }else{
            //Login function here returns user object
            //TODO: Either save the user in the login function or after it returns on this main thread..
            ioScope.launch {
                API_Interactor.handleAccountLogin(username, password)
                startActivity(intent)
            }

        }

    }


}
