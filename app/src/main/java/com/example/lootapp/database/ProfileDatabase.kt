package com.example.lootapp.database

import androidx.room.*
import com.example.lootapp.models.Profile
import java.util.*

// Interface for database CRUD operations
@Dao
interface ProfileDao{
    @Insert
    fun addProfile(profile: Profile) // C

    @Query("select * from profile" )
    fun getProfiles(): Profile // R

    @Update
    fun updateProfile(profile: Profile) // U

    @Delete
    fun deleteProfile(profile: Profile) // D
}



// Database class (instantiating database)
@Database(entities = arrayOf(Profile::class), version = 4, exportSchema = false)
abstract class ProfileDatabase : RoomDatabase(){
    abstract fun profileDao(): ProfileDao
}