package com.example.lootapp.database

import android.content.Context
import androidx.room.Room
import com.example.lootapp.models.Profile

class ProfileDatabaseRepository(ctx: Context) {

    private val profileList: MutableList<Profile> = mutableListOf()
    private val db: ProfileDatabase


    // Initialize database
    init {
       db = Room.databaseBuilder(
           ctx,
           ProfileDatabase::class.java,
           "profiles.db"
       ).fallbackToDestructiveMigration().allowMainThreadQueries().build()

        val profiles = db.profileDao().getProfiles()
        profileList.add(profiles)

    }


    // Get a profile
    fun getProfile(idx: Int): Profile {
        return profileList.get(idx)
    }

    // Add profile
    fun addProfile(profile: Profile){
      db.profileDao().addProfile(profile)
    }

    // Change username
    fun changeUsername(username: String, profile: Profile){
        profile.username = username
        db.profileDao().updateProfile(profile)
    }
    // Change password
    // Update profile??





}