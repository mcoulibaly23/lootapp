package com.example.lootapp.datasources


import com.example.lootapp.models.Bill

class BillsDataSource {
    companion object{

        fun createDataSet(): ArrayList<Bill>{
            val list = ArrayList<Bill>()
            list.add(
                Bill(
                    23.26,
                    "Comcast",
                    "April 23, 2020"

                )
            )
            list.add(
                Bill(
                    56.98,
                    "Verizon",
                    "March 13, 2020"

                )
            )


            list.add(
                Bill(
                    243.56,
                    "Audi",
                    "December 3, 2020"

                )
            )
            return list
        }
    }
}