package com.example.lootapp.datasources

import com.example.lootapp.models.Transaction

class DataSource {
    companion object{

        fun createDataSet(): ArrayList<Transaction>{
            val list = ArrayList<Transaction>()
            list.add(
                Transaction(
                    "Type 1",
                    "$500.91",
                    "Gucci",
                    "January 17, 2020",
                    group = "Group 1"
                )
            )
            list.add(
                Transaction(
                    "Type 2",
                    "$23.85",
                    "CVS",
                    "March 11, 2020",
                    group = "Group 2"
                )
            )


            list.add(
                Transaction(
                    "Type 3",
                    "$236.57",
                    "Best Buy",
                    "April 22, 2020",
                    group = "Group 3"
                )
            )
            list.add(
                Transaction(
                    "Type 4",
                    "$1.23",
                    "Dollar Tree",
                    "Apri; 22, 2020",
                    group = "Group 4"
                )
            )
            list.add(
                Transaction(
                    "Type 5",
                    "$67.32",
                    "Gamestop",
                    "April 23, 2020",
                    group = "Group 5"
                )
            )
            list.add(
                Transaction(
                    "Type 6",
                    "$12.34",
                    "Macy's",
                    "May 1, 2020",
                    group = "Group 6"
                )
            )

            return list
        }
    }
}