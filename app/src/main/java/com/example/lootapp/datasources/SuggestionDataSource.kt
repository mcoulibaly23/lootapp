package com.example.lootapp.datasources

import com.example.lootapp.models.Bill
import com.example.lootapp.models.Suggestion

class SuggestionDataSource {
    companion object{
        fun createDataSet(): ArrayList<Suggestion>{
            val list = ArrayList<Suggestion>()
            list.add(
                Suggestion(
                    "Suggestion 1"

                )
            )
            list.add(
                Suggestion(
                    "Suggestion 2"

                )
            )
            list.add(
                Suggestion(
                    "Suggestion 3"

                )
            )
            return list
        }
    }
}