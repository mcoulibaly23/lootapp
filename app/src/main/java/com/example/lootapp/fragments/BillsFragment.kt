package com.example.lootapp.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lootapp.datasources.BillsDataSource
import com.example.lootapp.R
import com.example.lootapp.adapters.BillAdapter
import kotlinx.android.synthetic.main.fragment_bills.*

/**
 * A simple [Fragment] subclass.
 */
class BillsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bills, container, false)
    }

    // Displays Bill RecyclerView in fragment
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = BillsDataSource.createDataSet()
        val adapter = BillAdapter()
        adapter.submitList(data)
        billsRecyclerView.adapter = adapter
        billsRecyclerView.layoutManager = LinearLayoutManager(context)
    }


}
