package com.example.lootapp.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.lootapp.R
import kotlinx.android.synthetic.main.fragment_suggestion_two.*

/**
 * A simple [Fragment] subclass.
 */
class SuggestionTwo : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_suggestion_two, container, false)
    }


}
