package com.example.lootapp.api

import com.example.lootapp.models.User
import com.example.lootapp.models.UserAccountInfo
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

class UserDeserializer: ResponseDeserializable<User> {
    override fun deserialize(content: String): User? = Gson().fromJson(content, User::class.java)

}


class AccountInfoDeserializer: ResponseDeserializable<UserAccountInfo> {
    override fun deserialize(content: String): UserAccountInfo? = Gson().fromJson(content, UserAccountInfo::class.java)
}