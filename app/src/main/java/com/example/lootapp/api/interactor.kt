package com.example.lootapp.api

import android.accounts.Account
import com.example.lootapp.datasources.UserDeserializer
import com.example.lootapp.models.GroupDepositQuery
import com.example.lootapp.models.GroupExpenseQuery
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.result.Result
import com.github.kittinunf.fuel.core.extensions.jsonBody
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject

class API_Interactor {
    companion object {
        suspend fun handleAccountLogin(username: String, password: String): Any? {

            return withContext(Dispatchers.IO){
                val reqBody = JSONObject()
                reqBody.put("username", username)
                reqBody.put("password", password)
                //TODO: change the ip address to your personal when hitting the api to get this to work...
                val (request,response, result) = Fuel.post(
                    "https://lootapi.herokuapp.com/login"
                ).header("Content-type" to "application/json")
                    .jsonBody(reqBody.toString())
                    .also { println(it) }
                    .responseObject(UserDeserializer())

                when (result) {
                    is Result.Failure -> {
                        println("Inside the Fuel Failure result  ${response.body().length}")
                        return@withContext String(response.data)
                    }
                    is Result.Success -> {
                        val data = result.get()
                        println("Inside the Fuel Data Success result ${data}") // you can also use ${String(response.data)
                        return@withContext data // response.statusCode is also available if we need to go that path.
                    }
                }
            }
        }

        suspend fun fetchUserAccountInfo(username: String): Any{
            return withContext(Dispatchers.IO ){
                val (request,response,result) =
                    Fuel.get("http://10.0.0.21:3000/account_info/${username}")
                    .header("Content-type" to "application/json")
                        .responseObject(AccountInfoDeserializer())

                when (result) {
                    is Result.Failure -> {
                        println("Inside the Fuel Failure result  ${response.body().length}")
                        return@withContext String(response.data)
                    }
                    is Result.Success -> {
                        val data = result.get()
                        println("Inside the Fuel Data Success result ${data}") // you can also use ${String(response.data)
                        return@withContext data // response.statusCode is also available if we need to go that path.
                    }
                }

            }
        }

        suspend fun linkNewUserAccount(username:String, accountNumber: String):Any{
            return withContext(Dispatchers.IO){
                val (request,response,result) =
                    Fuel.post("http://10.0.0.21:3000/account_info/${username}/link?account_number=${accountNumber}")
                        .header("Content-type" to "application/json")
                        .response()

                }
        }

        suspend fun unlinkNewUserAccount(username:String, accountNumber: String):Any{
            return withContext(Dispatchers.IO){
                val (request,response,result) =
                    Fuel.delete("http://10.0.0.21:3000/account_info/${username}/link?account_number=${accountNumber}")
                        .header("Content-type" to "application/json")
                        .response()

            }
        }

        suspend fun groupExpense(username:String , transactionInfo: GroupExpenseQuery){
            withContext(Dispatchers.IO){
                val reqBody = JSONObject()
                reqBody.put("info", transactionInfo)
                val (request,response,result) =
                    Fuel.post("http://10.0.0.21:3000/account_info/${username}/group")
                        .header("Content-type" to "application/json")
                        .jsonBody(reqBody.toString())
                        .response()

            }
        }

        suspend fun groupDeposit(username:String , transactionInfo: GroupDepositQuery){
            withContext(Dispatchers.IO){
                val reqBody = JSONObject()
                reqBody.put("info", transactionInfo)
                val (request,response,result) =
                    Fuel.post("http://10.0.0.21:3000/account_info/${username}/group")
                        .header("Content-type" to "application/json")
                        .jsonBody(reqBody.toString())
                        .response()
            }
        }
    }
}


