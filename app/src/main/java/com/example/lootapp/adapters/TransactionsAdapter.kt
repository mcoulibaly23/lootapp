package com.example.lootapp.adapters

import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.lootapp.R
import com.example.lootapp.models.Transaction
import kotlinx.android.synthetic.main.transactions_list_item.view.*

class TransactionsAdapter : RecyclerView.Adapter<TransactionViewHolder>() {
    // Holds all transactions
    var items : List<Transaction> = ArrayList()

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): com.example.lootapp.adapters.TransactionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.transactions_list_item, parent, false)
        return com.example.lootapp.adapters.TransactionViewHolder(view)

    }

    fun submitList(transactionList: List<Transaction>){
        items = transactionList
    }


    override fun onBindViewHolder(
        holder: com.example.lootapp.adapters.TransactionViewHolder,
        position: Int
    ) {
        val transaction = items.get(position)
        holder.bind(transaction)
    }




}

class TransactionViewHolder(v:View) : RecyclerView.ViewHolder(v) {
    fun bind(transaction: Transaction){
        itemView.amount.text = transaction.amount.toString()
        itemView.party.text = transaction.party
        itemView.date.text = transaction.date

    }
}