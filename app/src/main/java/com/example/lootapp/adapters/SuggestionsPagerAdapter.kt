package com.example.lootapp.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.lootapp.fragments.SuggestionOne
import com.example.lootapp.fragments.SuggestionThree
import com.example.lootapp.fragments.SuggestionTwo

class SuggestionsPagerAdapter (fm: FragmentManager): FragmentPagerAdapter(fm) {
    // Returns the number of tabs
    override fun getCount(): Int {
        return 3
    }

    // Returns the fragment we want to display
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                SuggestionOne()
            }
            1 -> SuggestionTwo()
            else -> {
                return SuggestionThree()
            }
        }
    }

    // Giving the tabs a title
    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Suggestion 1"
            1 -> "Suggestion 2"
            else -> {
                return "Suggestion 3"
            }
        }
    }

}