package com.example.lootapp.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.lootapp.R
import com.example.lootapp.models.Bill
import kotlinx.android.synthetic.main.transactions_list_item.view.amount
import kotlinx.android.synthetic.main.transactions_list_item.view.party
import kotlinx.android.synthetic.main.transactions_list_item.view.date

class BillAdapter : RecyclerView.Adapter<BillViewHolder>() {


    // Holds all transactions
    var items : List<Bill> = ArrayList()

    // Creates view
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): com.example.lootapp.adapters.BillViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.bill_list_item, parent, false)
        return com.example.lootapp.adapters.BillViewHolder(view)

    }

    // Binds specific item to view
    override fun onBindViewHolder(holder: BillViewHolder, position: Int) {
        val bill = items.get(position)
        holder.bind(bill)
    }

    // Returns number of bills
    override fun getItemCount(): Int {
        return items.count()
    }

    // Populates list
    fun submitList(billList: List<Bill>){
        items = billList
    }

}

// Logic to bind the data to the view
class BillViewHolder(v:View) : RecyclerView.ViewHolder(v) {
    fun bind(bill: Bill){
        itemView.amount.text = bill.amount.toString()
        itemView.party.text = bill.party
        itemView.date.text = bill.dueDate

    }
}