package com.example.lootapp.models

// Transaction model
data class Transaction(
    var type: String,
    var amount: String,
    var party: String,
    var date: String,
    var group: String
) {

}