package com.example.lootapp.models

data class AccountInfo(
    var type: String,
    var balance: String,
    var currency: String,
    var accountNumber: String,
    var expDate: String,
    var issuer: String
)

data class ExtendedAccountInfo (
    var type: String,
    var balance: String,
    var currency: String,
    var accountNumber: String,
    var expDate: String,
    var issuer: String,
    var Transactions: Array<Transaction>
)