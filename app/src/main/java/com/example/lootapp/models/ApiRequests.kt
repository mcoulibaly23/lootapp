package com.example.lootapp.models

data class GroupExpenseQuery (
    val type: String = "expense",
    val party: String,
    val accountNumber: String,
    val group: String
)

data class GroupDepositQuery (
    val type: String = "deposit",
    val party: String,
    val accountNumber: String,
    val group: String
)