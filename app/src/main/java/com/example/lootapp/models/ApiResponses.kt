package com.example.lootapp.models


data class UserAccountInfo (
    var username:String,
    var name: String,
    var linkedAccounts: Array<ExtendedAccountInfo>
)