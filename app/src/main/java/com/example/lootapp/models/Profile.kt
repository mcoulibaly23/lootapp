package com.example.lootapp.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import java.util.*
// Basic data object for a profile

@Entity
data class Profile (
    @PrimaryKey
    var id: Double  = Math.random(),
    @ColumnInfo(name="name")
    var name: String,
    @ColumnInfo(name="username")
    var username: String,
    @ColumnInfo(name ="password")
    var password: String,
    @ColumnInfo(name ="num_accounts")
    var numOfAccounts: Int

){
}

