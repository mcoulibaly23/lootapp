package com.example.lootapp.models

data class Bill (
    var amount: Double,
    var party: String,
    var dueDate: String
){}