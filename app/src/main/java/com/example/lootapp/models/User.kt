package com.example.lootapp.models

data class User (
    var username:String,
    var name: String,
    var linkedAccounts: Array<AccountInfo>
)