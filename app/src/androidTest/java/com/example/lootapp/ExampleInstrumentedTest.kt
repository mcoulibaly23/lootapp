package com.example.lootapp

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.View
import androidx.test.InstrumentationRegistry
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.runner.AndroidJUnit4
import com.example.lootapp.activities.MainActivity
import org.hamcrest.Matcher
import org.hamcrest.Matchers.contains
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @get:Rule
    var activityRule: ActivityTestRule<MainActivity>
            = ActivityTestRule(MainActivity::class.java)

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.lootapp", appContext.packageName)
    }

    @Test
    fun testLoginFields(){

        val testUsername = "User"
        val testPassword  = "Password"

        onView(withId(R.id.editText))
            .perform(typeText(testUsername))
        onView(withId(R.id.editText3))
            .perform(typeText(testPassword))
        onView(withId(R.id.editText))
            .check(matches(withText(testUsername)))
        onView(withId(R.id.editText3))
            .check(matches(withText(testPassword)))
    }

    @Test
    fun testLogin(){

        onView(withId(R.id.editText))
            .perform(typeText("dnsoes1"))
        onView(withId(R.id.editText3))
            .perform(typeText("password"))
        onView(withId(R.id.login))
            .perform(click())
        Thread.sleep(500)
        onView(withId(R.id.homeContainer))
            .check(matches(isDisplayed()))

    }

    @Test
    fun testLoginFailure(){

        onView(withId(R.id.editText))
            .perform(typeText(""))
        onView(withId(R.id.editText3))
            .perform(typeText(""))
        onView(withId(R.id.login))
            .perform(click())
        onView(withId(R.id.failedText))
            .check(matches(isDisplayed()))

    }

    @Test
    fun testBillsDisplay(){
        tLogin()

        onView(withId(R.id.billsButton))
            .perform(click())
        onView(withId(R.id.billsRecyclerView))
            .check(matches(isDisplayed()))
    }

    @Test
    fun testTransactionDisplay(){
        tLogin()

        onView(withId(R.id.transactionsButton))
            .perform(click())
        onView(withId(R.id.transactionsRecyclerView))
            .check(matches(isDisplayed()))

    }

    @Test
    fun testSuggestionDisplay(){
        tLogin()

        onView(withId(R.id.suggestionsButton))
            .perform(click())
        onView(withId(R.id.suggestionsVP))
            .check(matches(isDisplayed()))

    }

    @Test
    fun testProfileDisplay(){
        tLogin()

        onView(withId(R.id.profileButton))
            .perform(click())
        onView(withId(R.id.profilePage))
            .check(matches(isDisplayed()))

    }

    /*@Test
    fun testUploadImage(){
        tLogin()

        onView(withId(R.id.profileButton))
            .perform(click())
        onView(withId(R.id.uploadImage))
            .perform(click())

    }*/

    @Test
    fun testChangeUsername(){
        tLogin()

        val newUsername = "new Username"

        onView(withId(R.id.profileButton))
            .perform(click())
        onView(withId(R.id.userNameBtn))
            .perform(click())
        onView(withId(R.id.newUser))
            .perform(typeText(newUsername))
        onView(withId(R.id.submit))
            .perform(click())
        onView(withId(R.id.username))
            .check(matches(withText(newUsername)))

    }


    @Test
    fun testChangePassword(){
        tLogin()

        val newPassword = "new Password"

        onView(withId(R.id.profileButton))
            .perform(click())
        Thread.sleep(500)
        clickbutton()
        onView(withId(R.id.newPass))
            .perform(typeText(newPassword))
        onView(withId(R.id.submit))
            .perform(click())
        onView(withId(R.id.password))
            .check(matches(withText(newPassword)))

    }

    @Test
    fun testSuggestion1(){

        tLogin()

        onView(withId(R.id.suggestionsButton))
            .perform(click())
        onView(withText("Suggestion 1"))
            .perform(click())
        onView(withId(R.id.suggestionOneText))
            .check(matches(isDisplayed()))

    }

    @Test
    fun testSuggestion2(){

        tLogin()

        onView(withId(R.id.suggestionsButton))
            .perform(click())
        onView(withText("Suggestion 2"))
            .perform(click())
        onView(withId(R.id.suggestionTwoText))
            .check(matches(isDisplayed()))

    }

    @Test
    fun testSuggestion3(){

        tLogin()

        onView(withId(R.id.suggestionsButton))
            .perform(click())
        onView(withText("Suggestion 3"))
            .perform(click())
        onView(withId(R.id.suggestionThreeText))
            .check(matches(isDisplayed()))

    }

    /*@Test
    fun testToolBar(){

        tLogin()

        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());

        onView(withId(R.id.nav_header_textView))
            .check(matches(isDisplayed()))
    }*/

    private fun tLogin(){
        onView(withId(R.id.editText))
            .perform(typeText("dnsoes1"))
        onView(withId(R.id.editText3))
            .perform(typeText("password"))
        onView(withId(R.id.login))
            .perform(click())
        Thread.sleep(5000)
    }

    fun handleConstraints(action: ViewAction, constraints: Matcher<View>): ViewAction? {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return constraints
            }

            override fun getDescription(): String {
                return action.description
            }

            override fun perform(uiController: UiController?, view: View?) {
                action.perform(uiController, view)
            }
        }
    }

    fun clickbutton() {
        onView(withId(R.id.passwordBtn))
            .perform(handleConstraints(click(), isDisplayingAtLeast(65)))
    }
}
